package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s URL\n", os.Args[0])
		return
	}

	url := os.Args[1]

	caCert, err := ioutil.ReadFile("server.crt")
	if err != nil {
		panic(err)
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	cert, err := tls.LoadX509KeyPair("client.crt", "client.key")
	if err != nil {
		panic(err)
	}

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:            caCertPool,
				InsecureSkipVerify: true,
				Certificates:       []tls.Certificate{cert},
			},
		},
	}

	resp, err := client.Get(url)
	if err != nil {
		panic(err)
	}

	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	resp.Body.Close()
	fmt.Printf("Status: %s\n", resp.Status)
	fmt.Print(string(respData))

}
